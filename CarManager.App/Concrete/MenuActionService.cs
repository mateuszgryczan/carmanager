﻿using CarManager.Domain.Entity;
using System.Collections.Generic;


namespace CarManager.App.Concrete
{
    public class MenuActionService
    {
        public List<MenuAction> MenuActions;

        public MenuActionService()
        {
            MenuActions = new List<MenuAction>();
        }

        public List<MenuAction> GetMenuActionsByMenuName(string menuName)
        {
            List<MenuAction> menuActions = new List<MenuAction>();

            foreach(MenuAction menuAction in MenuActions)
            {
                if(menuAction.MenuName == menuName)
                {
                    menuActions.Add(menuAction);
                }
            }

            return menuActions;
        }

        private void AddActionToList(int id, string name, string menuName)
        {
            MenuAction menuAction = new MenuAction() { Id = id, Name = name, MenuName = menuName };

            MenuActions.Add(menuAction);
        }

    }
}
